export interface Pringao {
    key: string;
    description: string;
    name: string;
    nivelPringao: string;
    palitos: number;
}
