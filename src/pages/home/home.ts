import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Pringao } from '../../app/entitys/pringao';
import { PringaosServiceProvider } from '../../providers/pringaos-service/pringaos-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PringaosServiceProvider]
})
export class HomePage {
  addPringao: boolean = false;
  pringaos: Pringao[] = [];
  pringao: Pringao;
  constructor(public navCtrl: NavController,public pringaosRest: PringaosServiceProvider) {
    this.pringao = {
      key: '',
      description: '',
      name: '',
      nivelPringao: '',
      palitos: 0
    };
  }

  showPringao = () => {
    console.log('showPringaos', this.pringaos);
    this.pringaosRest.getPringaos().subscribe(data => {
      this.pringaos = Object.keys(data).map((index) => {
        const pringao: Pringao = {
          key: index,
          description: data[index].description,
          name: data[index].name,
          nivelPringao: data[index].nivelPringao,
          palitos: data[index].palitos
        };
        return pringao;
      });

      console.log('los pringaos', this.pringaos);
    }
  );
  }

  addPalito = (data: Pringao) => {
     data.palitos += 1;
  }

  removePalito = (data: Pringao) => {
    data.palitos -= 1;
  }

  updatePringao = (data: Pringao) => {
    console.log('updatePringao', data);
    this.pringaosRest.updatePringao(data).subscribe(res => {
      console.log('updated', res);
    });
  }

}
