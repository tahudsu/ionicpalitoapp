import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Pringao } from "../../../src/app/entitys/pringao";
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
/*
  Generated class for the PringaosServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PringaosServiceProvider {
  firebaseUrl = 'https://demoyesus-7c3d3.firebaseio.com';

  constructor(public http: Http) {
    console.log('Hello PringaosServiceProvider Provider');
  }

  getPringaos() {

    return this.http.get(this.firebaseUrl +  '/pringaos.json', { withCredentials: false })
    .map(response => {
      console.log(response.json());
      return response.json();
    });
  }

  savePringao(data: Pringao) {
    delete data.key;
    const body = JSON.stringify(data);

    return this.http.post(this.firebaseUrl +  '/pringaos.json', body, { withCredentials: false })
    .map(response => {
      console.log(response.json());
      return response.json();
    });
  }

  updatePringao(data: Pringao) {
    const key: string = data.key.toString();
    const dataToTravel = {};
    dataToTravel[key] = data;
    delete data.key;
    const body = JSON.stringify(dataToTravel);

    return this.http.patch(this.firebaseUrl +  '/pringaos.json', body, { withCredentials: false })
    .map(response => {
      console.log(response.json());
      return response.json();
    });
  }
}
